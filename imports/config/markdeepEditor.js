let mobilePreviewPortraitAsDefault = true;
let markdeepHelpLink = "https://casual-effects.com/markdeep/features.md.html#basicformatting";

module.exports = {
	mobilePreviewPortraitAsDefault,
	markdeepHelpLink
};
